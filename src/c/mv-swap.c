#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/syscall.h>
// Taken from https://unix.stackexchange.com/questions/32894/best-way-to-swap-filenames, SO user @Tenders McChiken
// See also: https://gist.github.com/eatnumber1/f97ac7dad7b1f5a9721f

// Ubuntu 18.04 does not define RENAME_EXCHANGE
// Value obtained manually from '/usr/include/linux/fs.h'
// You should switch to RENAME_EXCHANGE on modern systems
// Just remove the following line, then remove the `local_`
// prefix where it appears later in this function.
int local_RENAME_EXCHANGE = (1 << 1);

int main(int argc, char **argv) {
	if (argc != 3) {
		if (argc == 2) {
			if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
				printf("Swap names of two files pass as arguments\n");
				printf("Usage: %s PATH1 PATH2\n", argv[0]);
				return 0;
			}
		}
		fprintf(stderr, "Error: Could not swap names. Usage: %s PATH1 PATH2\n", argv[0]);
		return 2;
	}
	int r = syscall(
		SYS_renameat2,
		AT_FDCWD, argv[1],
		AT_FDCWD, argv[2],
		local_RENAME_EXCHANGE
	);
	if (r < 0) {
		perror("Error: Could not swap names");
		return 1;
	}
	else return 0;
}
