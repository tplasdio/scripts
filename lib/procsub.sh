#!/bin/sh
# Adapted from https://github.com/modernish/modernish. License: CC0
# Copyright (c) 2015-2022 Martijn Dekker <martijn@inlv.org>

procsub() {
	unset -v _procsub_o

	while case ${1-} in
	(-o) _procsub_o= ;;
	(--) shift; break ;;
	(*) break
	esac; do
		shift
	done

	[ $# = 0 ] && return 3
	[ $1 = '' ] && return 4
	: & _procsub_fifo="${TMPDIR:-"/tmp"}/fifo_${$}_$!"

	until (exec mkfifo "${_procsub_fifo}") 2>/dev/null; do
		[ $? -ne 0 ] && return 2
	done 1>&1

	(
		command : 1>&2 && exec 1>&2 || exec 1>&-
		command trap "PATH=/bin:/usr/bin; unset -f rm; exec rm -f ${_procsub_fifo} 2>/dev/null" 0 PIPE INT TERM
		case ${1-} in
		(command) shift
			[ "${_procsub_o+x}" ] \
				&& command "$@" < "${_procsub_fifo}" \
				|| command "$@" > "${_procsub_fifo}"
		;;
		(*)
			[ "${_procsub_o+x}" ] \
				&& "$@" < "${_procsub_fifo}" \
				|| "$@" > "${_procsub_fifo}"
		esac
	) & printf -- "%s\n" "${_procsub_fifo}"
}

#alias %=procsub
