#!/bin/sh
# Comando que prenda o apague interfaces
set_interface_switch_cmd() {
if hash ifconfig >/dev/null 2>&1; then
	interface_switch_cmd(){ "${_SUPER:-sudo}" ifconfig "$@" ;}
elif hash ip >/dev/null 2>&1; then
	interface_switch_cmd(){ "${_SUPER:-sudo}" ip link set dev "$@" ;}
else
	>&2 printf -- "No hay un comando que apague y prenda interfaces!\nInstale 'ip' o 'ifconfig'"
	return 1
fi
}
