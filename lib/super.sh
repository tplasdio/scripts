#!/bin/sh
_which(){ hash "$1" >/dev/null 2>&1 && command -v -- "$1";}
: "${_SUDO:="$(_which sudo)"}"
: "${_DOAS:="$(_which doas)"}"
: "${_SUPER:="${_DOAS:-"${_SUDO}"}"}"
