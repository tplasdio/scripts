#!/bin/sh
# Todavía falta probar varias funciones
# Dependencias por el momento: fzf para restaurar
: "${BACKUP_HOME:="${HOME}/rsp"}"

respaldar() {
BACKUP_TIME="$(date +%s)"
BACKUP_DIR="${BACKUP_HOME}/pacman/${BACKUP_TIME}"

mkdir -p "$BACKUP_DIR" && printf "%s\n" "$BACKUP_DIR" || return 2

printf "Generando lista de paquetes de pacman en \033[1;32m%s/pacman.txt\033[m\n" "${BACKUP_DIR}"
pacman -Qqe | grep -v "$(pacman -Qqm)" > "${BACKUP_DIR}/pacman.txt"

printf "Generando lista de paquetes extranjeros (AUR) en \033[1;32m%s/pacman_aur.txt\033[m\n" "${BACKUP_DIR}"
pacman -Qqm > "${BACKUP_DIR}/pacman_aur.txt"

if [ "$OPT_SELF_DEPS" ]; then
	if hash pactree >/dev/null 2>&1; then
		printf "Generando lista de dependencias de pacman en \033[1;32m%s/pacman_self_dep.txt\033[m\n" "${BACKUP_DIR}"
		pacman -Qq $(pactree -u pacman) > "${BACKUP_DIR}/pacman_self_dep.txt"
	else
		printf "¡Debes instalar pactree en tu PATH!\n"
	fi
fi

if [ "$OPT_PACMAN_DB" ]; then
	printf "Archivando base de datos de pacman en \033[1;32m%s/pacman_db.tar.xz\033[m\n" "${BACKUP_DIR}"
	tar -caf "${BACKUP_DIR}/pacman_db.tar.xz" "/var/lib/pacman/local"
fi

if [ "$OPT_PACMAN_CONF" ]; then
	if [ -e "/etc/pacman.conf" ] && [ -f "/etc/pacman.conf" ]; then
		printf "Copiando pacman.conf en \033[1;32m%s/pacman.conf\033[m\n" "${BACKUP_DIR}"
		cp /etc/pacman.conf "${BACKUP_DIR}/pacman.conf" \
			|| return 3
	else
		printf "pacman.conf no encontrado\n"
	fi
fi
}

listar_backups() {
backups=$(ls "$BACKUP_HOME/pacman/" | grep -E '^[0-9]{9,11}$') || return 3
for backup in ${backups}; do
	printf '%s\t%s\n' "$backup" "$(date --date=@$backup '+%d/%m/%Y,%H:%M:%S')"
done | sed "s|^|$BACKUP_HOME/pacman/|"
}

restaurar() {
if [ "$1" ]; then
	BACKUP_DIR=$(realpath "$1")
else
	listar_backups >/dev/null

	if [ "$backups" ]; then
		if hash fzf >/dev/null 2>&1; then
			backup=$(fzf --preview 'date --date=@{1} "+%d/%m/%Y %H:%M:%S"' <<-eof
			$backups
			eof
			)
			BACKUP_DIR="$BACKUP_HOME/pacman/$backup"
		else
			# TODO: poner una prompt para que ingrese el directorio correcto
			return 3
		fi
	else
		printf "No se encontraron respaldos\n"
	fi
fi

if [ "$OPT_REINSTALAR" ]; then
	# Reinstalar de la lista de paquetes respaldados
	# Atención! Todavía no probado
	cat "$BACKUP_DIR/pacman.txt" | xargs pacman -S --needed --noconfirm
elif [ "$OPT_PACMAN_DB_R" ]; then
	# Restaurar base de datos de pacman
	# Atención! Todavía no probado
	[ -f "$BACKUP_DIR/pacman_database.tar.bz2" ] \
	 && cd / && tar -xvf "$BACKUP_DIR/pacman_database.tar.bz2" \
	 || return 4
fi
}

uso() {
cat<<eof | column -t -s '	'
pacman-backup	Generar listas de paquetes instalados
pacman-backup -s	Además generar lista de dependencias de pacman
pacman-backup -b	Además respaldar base de datos de pacman
pacman-backup -c	Además respaldar el pacman.conf
pacman-backup -l	Listar directorios de respaldo
pacman-backup -r	Reinstalar de lista de paquetes instalados
pacman-backup -r <dir>	Reinstalar de lista de paquetes instalados en cierto directorio
pacman-backup -rp	Restaurar la base de datos de pacman
pacman-backup --help	Mostrar esta ayuda
eof
}

if [ $# -eq 0 ]; then
 respaldar || exit $?
fi

while [ $# -gt 0 ]; do
	arg="$1"
	case "$arg" in
	(-s) OPT_SELF_DEPS=1 ;;
	(-b) OPT_PACMAN_DB=1 ;;
	(-b) OPT_PACMAN_DB=1 ;;
	(-c) OPT_PACMAN_CONF=1 ;;
	(-l) listar_backups || exit $?;;
	(-r) OPT_REINSTALAR=1
		shift
		restaurar "$@" || exit $?
		break;;
	(-rp) OPT_PACMAN_DB_R=1
		shift
		restaurar "$@" || exit $?
		break ;;
	-h|--help) uso;;
	(*) uso 1>&2; exit 1;;
	esac
	shift
done

if [ "$OPT_SELF_DEPS" ] || [ "$OPT_PACMAN_DB" ] || [ "$OPT_PACMAN_CONF" ]; then
	respaldar || exit $?
fi
