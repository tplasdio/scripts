#!/bin/sh
# Aleatorizar dirección MAC, requiere 'macchanger'
# Dependencies: macchanger, ifconfig

. "${XDG_DATA_HOME%/*}/lib/super.sh"
. "${XDG_DATA_HOME%/*}/lib/interface_switch_cmd.sh"

cambiar_mac() {
	_interface="$1"
	# Si la interfaz está prendida, apagarla, cambiar y prenderla
	interface_switch_cmd "${_interface}" down
	# También aleatorizar el bit U/L de la MAC con shuf
	"$_SUPER" macchanger -r$(shuf -e 'b' '' -n1) "${_interface}"
	# TODO: Solo prenderla si estaba prendida antes
	interface_switch_cmd "${_interface}" up
}

interactivo() {
	# Para cada interfaz excepto loopback, preguntar si desea cambiarle la MAC
	for interface in $(ls /sys/class/net | grep -v 'lo'); do
		printf -- "¿Desea aleatorizar interfaz '\033[1m%s\033[m'? (Sí|no): " "${interface}"
		read -r REPLY
		case "${REPLY}" in
		(''|[SsYy]*) cambiar_mac "$interface";;
		([Nn]*) continue;;
		esac
	done
}

main() {
	set_interface_switch_cmd || return $?
	case "$1" in
	(-y|--yes) yes | interactivo ;;
	(*) interactivo
	esac
}

main "$@" || exit $?
