#!/bin/sh
# Guion para cambiar el fondo de GRUB de este tema en cada reinicio
THEME_DIR='/boot/grub/themes/dark-matter'
BACKGROUND=$(shuf -e "${THEME_DIR}"/assets/background* -n1)
DISTRO="${BACKGROUND##"${BACKGROUND%_*}"?}" DISTRO="${DISTRO%.*}"
PROGRESS_HI_C=$(shuf -e "${THEME_DIR}/assets/progress_highlight_c_${DISTRO}"* -n1)

rm "${THEME_DIR}/background.png" "${THEME_DIR}/progress_highlight_c.png"
ln -s "$BACKGROUND" "${THEME_DIR}/background.png"
ln -s "$PROGRESS_HI_C" "${THEME_DIR}/progress_highlight_c.png"
