#!/bin/sh

# Optional dependency: bc (for more decimals)

battery="BAT0" \
decimals="2"  # validate if made into an option

if hash bc >/dev/null 2>&1; then
	charge_percentage() {
		percentage=$(bc -l <<-EOF
		scale=$decimals; 100*${now}/${full}
		EOF
		)
	}
else
	if [ "$decimals" -le 10 ]; then  # many shells don't support more digits on $(())
		charge_percentage() {
			num="${now}00$(printf "%${decimals}s"| tr " " "0")" \
			percentage=$((num/full)) \
			decimal_place="$((${#percentage} - decimals))" \
			percentage=$(sed 's|\('"$(printf "%${decimal_place}s"| tr " " ".")"'\)\(.*\)|\1.\2|' <<-EOF
			$percentage
			EOF
			)
			# maybe TODO: use printf %2f to adjust decimal places instead, then tr ',' '.'
		}
	else
		charge_percentage() {
			percentage="$((100*now/full))"
		}
	fi
fi

check_battery() {
	read -r capacity full now status <<-EOF
	$(paste -d' ' \
	/sys/class/power_supply/$battery/capacity \
	/sys/class/power_supply/$battery/charge_full \
	/sys/class/power_supply/$battery/charge_now \
	/sys/class/power_supply/$battery/status)
	EOF
	charge_percentage
}

main() {
	check_battery

	case "$1" in
	(-m|--monitor)
		if [ "${capacity}" -le 100 ] ; then
			printf -- "CHARGE%%\t\tCHARGE\tFULL\t\n"
			while :; do
				printf -- "\033[1;31m%s%%\033[32m\t\t%s\t%s\033[m\n" "${percentage}" "${now}" "${full} $(date '+%H:%M:%S')"
				# TODO: mostrar el ratio de carga y descarga; y una estimación de cuánto falta para 0% o 100%
				sleep 3
				check_battery
			done
		fi
	;;
	(*) printf -- "%s%%\n" "${percentage}"
	esac
}

main "$@" || exit $?
