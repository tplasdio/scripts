#!/bin/sh

# @(#) Rename files using a sed expression on each filename.

# For something more sophisticated, see perl-rename

usage() {
	_0="${0##*/}"
	echo "\
Rename files using a sed expression on each filename.

Usage:
  $_0 SEDEXPR FILE...

Environment variables:
  MV    # move command to use. Default: mv
  SED   # sed command to use. Default: sed

Examples:
  $_0 's/a/b/g' file           # Replace 'a's for 'b's in filename
  $_0 's/$/suffix/' files      # Add a suffix to filenames
  MV='mv -i' $_0 's/a//' file  # Rename with prompt before overwrite"

}

case "$1" in
(-h | --help)
	usage
	exit
esac

if [ $# -lt 2 ]; then
	>&2 usage
	exit 22
fi

: \
"${SED:=sed}" \
"${MV:=mv}"
unset -v IFS

CCn='
' \
sed_expression="$1"
shift

for file do
	if new_file="$($SED "$sed_expression" <<-EOF
	$file
	EOF
	)"
	then
		if ! [ "$file" = "$new_file" ]; then
			$MV -- "$file" "$new_file" || exit $?
		fi
	else
		>&2 printf -- '%s: Error in sed substitution, aborting\n' "${0##*/}"
		exit 2
	fi
done
