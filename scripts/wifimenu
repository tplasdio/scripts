#!/bin/sh

# Connect/Enable/Disable Wi-Fi from rofi/fzf/dmenu
# Dependencies: network-manager, nmcli, notify-send, fzf or rofi or dmenu

# TODO:
#	- Support other wifi connection utilities: iw, iwctl, etc.
#	- Type passwords with hidden characters in dmenu, fzf (rofi already has it)
#	- Show more information, like signal quality

CCn='
'

. "${XDG_DATA_HOME%/*}/lib/getoptions.sh"

fzf_cmd()  { prompt="${1:-"Wi-Fi: "}"; [ "$1" ] && shift; fzf --prompt "${prompt}" -m "$@"; }
rofi_cmd() { prompt="${1:-"Wi-Fi"}"; [ "$1" ] && shift; rofi -dmenu -i -p "${prompt}" "$@"; }
dmenu_cmd(){ prompt="${1:-"Wi-Fi"}"; [ "$1" ] && shift; dmenu -i -p "${prompt}" "$@"; }
rofi_cmd_passwd(){  [ "$1" ] && prompt="${1:-"Wi-Fi"}" && shift; rofi -dmenu -p "${prompt}" -password -l 0 "$@"; }

trim(){ set -- $*;printf -- '%s' "$*";}

parser() {
	setup REST help:usage -- "Seleccionar emojis con fzf/rofi/dmenu"
	flag Opt_rofi --rofi -- "Seleccionar con rofi"
	flag Opt_dmenu --dmenu -- "Seleccionar con dmenu"
	disp :usage -h --help -- "Mostrar esta ayuda"
}
eval "$(getoptions parser) exit 1"

if [ "$Opt_rofi" ]; then
	select_cmd='rofi_cmd' \
	select_cmd_passwd='rofi_cmd_passwd'
elif [ "$Opt_dmenu" ]; then
	select_cmd='dmenu_cmd'
else
	# fzf por omisión
	select_cmd='fzf_cmd'
fi

main() {

	case "$(nmcli -fields WIFI g)" in
	(*enabled*)

		notify-send "${0##*/}" "Getting Wi-Fi SSIDs"
		toggle_btn=" 睊 Disable Wi-Fi" \
		wifi_list=$(
		nmcli --fields "SECURITY,SSID" device wifi list \
			| sed -E '1d
				s/  */ /g
				s|WPA*.?\S|🔒|g
				s|^--|🔓|g
				/--/d' \
			| sed 's|🔒 🔒|🔒|g'
		) \
	;;

	(*disabled*)
		toggle_btn=" 直 Enable Wi-Fi"
		select_cmd_arg="-l 1"
	;;
	esac

	selected_network=$(
		printf -- "%s" "$toggle_btn$CCn$wifi_list" | "$select_cmd" "Wi-Fi" $select_cmd_arg
	)

	selected_id=${selected_network#????} \
	selected_id=$(trim "$selected_id")

	case "$selected_network" in
	('') return 2 ;;
	(' 直 Enable Wi-Fi')  nmcli radio wifi on  ;;
	(' 睊 Disable Wi-Fi') nmcli radio wifi off ;;
	(*)
		saved_connections=$(nmcli -g NAME connection)
		if grep -w "$selected_id" >/dev/null 2>&1 <<-EOF
			$saved_connections
			EOF
		then
			# Bug: if nm-applet is running, a window will pop up prompting for a password
			if ! nmcli connection up id "$selected_id"; then
				wifi_password=$("$select_cmd_passwd" "Password" || "$select_cmd" "Password")
				notify-send "${0##*/}" "Trying to establish connection"
				nmcli --ask connection up id "$selected_id" <<-EOF
				$wifi_password
				EOF
			else
				notify-send "Connection established" "Connected to $selected_id!"
			fi
		else
			case "$selected_network" in
			(🔒*) wifi_password=$("$select_cmd_passwd" "Password" || "$select_cmd" "Password")
			esac

			notify-send "${0##*/}" "Trying to establish connection"
			nmcli device wifi connect "$selected_id" password "$wifi_password" \
				&& notify-send "Connection established" "Connected to $selected_id!" \
				|| notify-send "Connection failed" "Not connected to $selected_id!"
		fi
	esac
}

main "$@" || exit $?
