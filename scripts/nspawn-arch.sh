#!/bin/sh

# Script para crear un contenedor systemd-nspawn de Arch, desde Arch
# Uso:
# nspawn-arch <nombre>
# nspawn-arch <nombre> "paquete1 paquete2"

#Bug: Debería validarse $OTHER_PACKAGES y $CONTAINER_NAME para evitar inyecciones, por el momento es vulnerable
#Bug: El directorio nspawn debería tener los permisos 755

. "${XDG_DATA_HOME%/*}/lib/super.sh" # Usar sudo o doas con "$_SUPER"

NSPAWN_DIR="${NSPAWN_DIR:-/var/lib/machines}"
CONTAINER_NAME="${1:-container"$(date +%s)"}"
CONTAINER_PATH="${NSPAWN_DIR}/${CONTAINER_NAME}"
OTHER_PACKAGES=${2:-"sudo neovim"}

if ! pacman -Qq arch-install-scripts >/dev/null 2>&1; then
	printf "Se instalará el paquete arch-install-scripts\n"
	"$_SUPER" pacman -S arch-install-scripts --nocofirm || exit 2
fi

"$_SUPER" mkdir -p "$CONTAINER_PATH" || exit 3
"$_SUPER" pacstrap -c "$CONTAINER_PATH" "base"

tmp_setup=$(mktemp)
cat <<\eof > "$tmp_setup"
#!/bin/sh
rm /etc/securetty
rm /usr/share/factory/etc/securetty
sed -i '/\[options\]/aNoExtract=/etc/securetty /usr/share/factory/etc/securetty' /etc/pacman.conf
sed -i '/#en_US\.UTF-8 UTF-8/s/#//' /etc/locale.gen # O tu localidad respectiva
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf # O tu localidad respectiva

pacman --noconfirm -Syu

printf "\033[1;32mNueva contraseña para root:\033[m\n"
passwd
printf "\033[1;32m¿Desea crear usuario y su directorio home? (Sí|no): \033[m"
read CREAR_USUARIO
case "$CREAR_USUARIO" in
(''|[SsYy]*)
	while ! [ "$USUARIO" ]; do
		printf "\n\033[1;32mNombre del usuario (sin espacios): \033[m"
		read USUARIO
	done
	printf "\n\033[1;32m¿Desea que esté en grupo 'wheel'? (Sí|no): \033[m"
	read WHEEL
	case "$WHEEL" in
	(''|[SsYy]*)
		useradd -m -G wheel -s /bin/bash "$USUARIO"
		sed -i '/%wheel ALL=(ALL) ALL/s/#//' /etc/sudoers
		;;
	([Nn]*) useradd -m -s /bin/bash "$USUARIO";;
	esac
	printf "\033[1;32mNueva contraseña para %s:\033[m\n" "${USUARIO}"
	passwd "$USUARIO"
	;;
esac
eof
sed -i "/^pacman/s/$/ $OTHER_PACKAGES&/" "$tmp_setup"

"$_SUPER" sh -c 'mv "'"$tmp_setup"'" "'"$CONTAINER_PATH"'/root/setup.sh"'
"$_SUPER" chmod 744 "$CONTAINER_PATH/root/setup.sh"
"$_SUPER" systemd-nspawn -D "$CONTAINER_PATH" /root/setup.sh

printf "\n\033[1;32m¿Desea arrancar el contenedor ahora? (Sí|no): \033[m"
read
case "$REPLY" in
(''|[SsYy]*)
	if hash snr >/dev/null 2>&1; then
		snr "$CONTAINER_PATH"
	else
		"$_SUPER" systemd-nspawn -bD "$CONTAINER_PATH"
	fi
esac
