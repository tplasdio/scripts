#!/bin/sh

# yay -S monero xmrig-bin p2pool

run_monero_node() {
	rpc_port=18089
	zmq_port=18084

	printf "\033[4;1mWarning:\033[m\n"\
"  This would download the Monero blockchain ~140 GB of space\n"\
"  First run could take from 6 hours to several days\n"\
"  Are you sure you want to continue? (Y|n): "
	read -r REPLY
	case "${REPLY}" in
	(''|[Yy]*);;
	([Nn]*) return 1
	esac

	set -x
	monerod \
		--zmq-pub tcp://127.0.0.1:"$zmq_port" \
		--rpc-bind-port "$rpc_port" \
		--out-peers 64 \
		--in-peers 32 \
		--add-priority-node=node.supportxmr.com:18080 \
		--add-priority-node=nodes.hashvault.pro:18080 \
		--disable-dns-checkpoints \
		--enable-dns-blocklist \
		"$@"
		# --prune-blockchain
	set +x
}

run_p2pool_node() {
	# Check https://xmrvsbeast.com/p2pool/monero_nodes.html for public nodes,
	# but I recommend running your own monerod daemon
	#host="127.0.0.1"
	host="" \
	rpc_port=18089 \
	zmq_port=18084
	# ↓ Should be your dedicated wallet primary address for mining, not subaddress
	wallet=""
	# TODO: prompt for variables if empty

	# This would take ~3 GB of RAM
	# Wait until it stops logging, when is ouputs 'BlockTemplate'
	set -x
	p2pool \
		--host "$host" \
		--rpc-port "$rpc_port" \
		--zmq-port "$zmq_port" \
		--wallet "$wallet" \
		"$@"
	set +x
}

mine_with_p2pool() {
	# Recommendation: enable huegepages and 1GB pages,
	# checkout xmrig documentation

	# This would take ~2.5 GB of RAM and get your CPU working
	ip="127.0.0.1"  # IP of the p2pool node
	set -x
	"${SUDO:-sudo}" xmrig -u x+"$difficulty" -o "$ip":3333 "$@"
	set +x
}


mine_with_config() {
	xmrig_config="/etc/xmrig.json"
	set -x
	"${SUDO:-sudo}" xmrig -u x+"$difficulty" -c "$xmrig_config" "$@"
	set +x
}

mine_subcmd() {
	# Note:
	#   hugepages are not released afterwards, you may
	#   release them manually with sudo sysctl -w vm.nr_hugepages=0

	difficulty=10000

	if [ $# -eq 0 ]; then
		mine_with_p2pool "$@" || return $?
	else
		case "$1" in
		(config)
			shift
			mine_with_config "$@" || return $? ;;
		(p2pool | '')
			shift
			mine_with_p2pool "$@" || return $?
		esac
	fi
}

main() {
	if [ $# -eq 0 ]; then
		mine_subcmd "$@"
	else
		case "$1" in
		(monerod)
			shift
			run_monero_node "$@" || return $? ;;
		(p2pool)
			shift
			run_p2pool_node "$@" || return $? ;;
		(xmrig | mine | '')
			shift
			mine_subcmd "$@" || return $? ;;
		(-h|--help)
			_0=${0##*/}
			cat <<-EOF
		$_0 monerod         # Run Monero node
		$_0 monerod <args>  # Run Monero node, with extra arguments
		$_0 p2pool          # Run p2pool node
		$_0 p2pool <args>   # Run p2pool node, with extra arguments
		$_0 xmrig p2pool    # Run xmrig (mine XMR) with p2pool node
		$_0 xmrig           # Same as ↑
		$_0                 # Same as ↑
		$_0 xmrig config    # Run xmrig (mine XMR) with your config
		EOF
		esac
	fi
}

main "$@" || exit $?
