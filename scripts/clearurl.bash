#!/usr/bin/bash
# This script clears urls from Google, DuckDuckGo search, Mozilla addons page
# Not taken from but shoutouts to https://docs.clearurls.xyz/
# Usage:
# clearurl.bash url1 url2 url3
# clearurl.bash file_of_urls

clearurl() {
shopt -u nullglob
for url in "$@"; do

#Google
local gog=$(grep -Po ".*(?<=&url=)" <<< "$url")
url=${url//$gog/}
local usa=$(grep -Po "(?=&usg).*" <<< "$url")
url=${url//$usa/}

#Duckduckgo
local uddg=$(grep -Po ".*(?<=uddg=)" <<< "$url")
url=${url//$uddg/}
local notrut=$(grep -Po "(?=&notrut).*" <<< "$url")
url=${url//$notrut/}
local rut=$(grep -Po "(?=&rut=).*" <<< "$url")
url=${url//$rut/}

#Mozilla
#Example: https://outgoing.prod.mozaws.net/v1/28dd9d7752506572347c59fb9ce05fda98cd5d604e59fce68e727060f3c9d1a6/https%3A//www.youtube.com/watch%3Uv=cGJx1Ll19Qi
#local mozaws=$(grep -Po "" <<< "$url")

#Decoding
# To check and improve: https://unix.stackexchange.com/questions/159253/decoding-url-encoding-percent-encoding
url=${url//%2F/\/}
url=${url//%3A/:}
printf "%s\n" "$url"
done
}

if [[ -f "$1" ]]; then
	readarray -t urls < "$1"
	clearurl "${urls[@]}"
else
	clearurl "$@"
fi
