#!/usr/bin/perl
# Ordenamiento numérico ascendente del último campo
my @lines=<STDIN>;
my $separator= $ARGV[0] || '\b';
print sort{($b=~/.*$separator(\d+)/)[0]<=>($a=~/.*$separator(\d+)/)[0]}@lines;
