#!/bin/sh
# Este script demuestra el uso de parámetros especiales
main() {
{
printf -- "\033[1;34m--------\t-----------\t-----\n"
printf -- "\033[32mVariable\tDescripción\tValor\n"
printf -- "\033[34m--------\t-----------\t-----\033[m\n"
printf -- '${0%%?"${0##*/?}"}'" \tDirectorio del script\t%s\n" "${0%%?"${0##*/?}"}"
printf -- '$0'" \tRuta del script\t%s\n" "$0"
printf -- '$1'" \t1° argumento\t%s\n" "$1"
printf -- '$2'" \t2° argumento\t%s\n" "$2"
printf -- '$3'" \t3° argumento\t%s\n" "$3"
printf -- '$#'" \tNúmero de argumentos\t%s\n" "$#"
printf -- '\${$#}'" \tÚltimo argumento\t%s\n" "$(eval echo \${$#})"
printf -- '\${$(($#-1))}'" \tPenúltimo argumento\t%s\n" "$(eval "echo \${$(($#-1<0?a:$#-1))}")"
printf -- '$@'" \tArreglo de argumentos\t"
printf -- "'%s' " "$@"; printf "\n"
printf -- '$*'" \tString de los argumentos\t'%s'\n" "$*"
printf -- '$-'" \tOpciones de shell\t%s\n" "$-"
printf -- '$$'" \tPID del último comando\t%s\n" "$$"
printf -- '$PPID'" \tPID del proceso que inició el script\t%s\n" "$PPID"
true '' & # Poner un trabajo en segundo plano
printf -- '$!'" \tPID del último trabajo en segundo plano\t%s\n" "$!"
(exit 40)
printf -- '$?'" \tÚltimo estado de salida\t%s\n" "$?"
printf -- '$_'" \tÚltimo argumento de último comando\t%s\n" "$_"
printf -- '$OPTIND'" \tÍndice del siguiente argumento a procesarse\t%s\n" "$OPTIND"
printf -- '$OPTARG'" \tValor del último argumento procesado\t%s\n" "$OPTARG"
} | column -t -s "	"
}

uso() {
cat <<EOF
Simple script para demostrar el uso de parámetros especiales del shell
USO: shellpars.sh <args>
EOF
}

case "$1" in
(-h|--help) uso; shift ;;
esac
main "$@"
