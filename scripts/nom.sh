#!/bin/sh
# Uso: nom.sh <ficheros>
# Info mejor formateada de ficheros

# Ver: https://stackoverflow.com/questions/3915040/how-to-obtain-the-absolute-path-of-a-file-via-shell-bash-zsh-sh
#		https://stackoverflow.com/questions/29832037/how-to-get-script-directory-in-posix-sh/29835459#29835459
vars_de_fichero() {
#Bug: directorio /bin en directorio raíz aparece como //bin
#Bug: directorio /bin al hacer 'cd' se entra en su enlace suave /usr/bin
directorio="$(cd "${1%?"${1##*/?}"}" 2>/dev/null; pwd -P)/"
fichero="${directorio}${1##*/}"
nombre="${1##*/}"
fichero_sin_ext="${nombre%%.*}"
extensiones=$([ "${nombre#*.}" != "$nombre" ] && printf ".%s" "${nombre#*.}")
fichero_real=$(readlink -f "$1")
directorio_real="${fichero_real%?"${fichero_real##*/?}"}"
nombre_real="${fichero_real##*/}"
fichero_real_sin_ext="${nombre_real%%.*}"
extensiones_real=$([ "${nombre_real#*.}" != "$nombre_real" ] && printf ".%s" "${nombre_real#*.}")
}

imprimir_rutas() {
{
printf "Fichero:\t%s\n" "${fichero}"
printf "Directorio:\t%s\n" "${directorio}"
printf "Nombre:\t%s\n" "${nombre}"
printf "Sin_extensiones:\t%s\n" "${fichero_sin_ext}"
printf "Extensiones:\t%s\n" "${extensiones}"
printf "Fichero_real:\t%s\n" "${fichero_real}"
printf "Directorio_real:\t%s\n" "${directorio_real}"
printf "Nombre_real:\t%s\n" "${nombre_real}"
printf "Sin_extensiones_real:\t%s\n" "${fichero_real_sin_ext}"
printf "Extensiones_real:\t%s\n" "${extensiones_real}"
} | column -t -s '	'
}

# TODO:
# - Que llame fzf si no hay argumentos (separarlos con byte nulo?)
# - Espaciado entre ficheros
# - Salida en CSV y JSON
# - Que sea una librería, no un script, que otros scripts puedan llamar a esas funciones

[ $# -eq 0 ] && exit 1
for f do
vars_de_fichero "$f"
imprimir_rutas
done

unset -f vars_de_fichero imprimir_rutas
