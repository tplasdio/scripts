#!/usr/bin/luajit
-- Usage:
-- cat fichero | trim       # Remueve blancos iniciales y finales
-- cat fichero | trim -c" " # Remueve espacios iniciales y finales
-- cat fichero | trim -r    # Remueve blancos finales
-- cat fichero | trim -l    # Remueve blancos iniciales
-- trim -r " uno dos"       # Remueve blancos iniciales de argumentos strings
-- trim -l "uno dos  "      # Remueve blancos finales de argumentos strings
-- trim "  uno dos  "       # Remueve blancos iniciales y finales de argumentos strings

-- TODO: aceptar ficheros a remover blancos sobrantes:
-- trim -f "fichero1" "fichero2"   # Remueve espacios iniciales y finales de ficheros argumentos

local stringx = require("pl.stringx")
local argparse = require("argparse")
stringx.import()

local VERSION = 0.1
local BLANKS = " \t\n\r\f\v"

function table.has_value(t,value)
	for _,e in pairs(t) do
		if e == value then
			return true
		end
	end
	return false
end

local function parse_cmdargs(cmdargs)
	local parser = argparse()
		:name "trim"
		:description "Trim text"
		:usage_margin(5)
		:help_description_margin(24)
		:help_max_width(200)
		:usage_max_width(200)

	parser:flag("--version")
		:description("Show version")
		:action(function() print(VERSION); os.exit(0) end)

	parser:flag("-r --right")
		:description("Trim from right")

	parser:flag("-l --left")
		:description("Trim from left")

	parser:option("-c --chars")
		:description("Characters to trim, blanks by default")

	--parser:flag("-f --files")
		--:description("Files to trim")
		--:args("+")

	parser:argument("string_args")
		:description("Strings to trim")
		:args("*")

	return parser:parse(cmdargs)
end

local function main()
	local args = parse_cmdargs(arg)

	args["chars"] = args["chars"] or BLANKS

	if #args["string_args"] == 0 or
		table.has_value(args["string_args"], "-")
	then
		-- TODO: print only after receiving EOF, not line break
		if args["left"] == args["right"] then
			for line in io.stdin:lines() do
				print(line:strip(args["chars"]))
			end
		elseif args["left"] then
			for line in io.stdin:lines() do
				print(line:lstrip(args["chars"]))
			end
		elseif args["right"] then
			for line in io.stdin:lines() do
				print(line:rstrip(args["chars"]))
			end
		end
	else
		for _,pp in ipairs(args["string_args"]) do
			if args["left"] == args["right"] then
				io.write(pp:strip(args["chars"]))
			elseif args["left"] then
				io.write(pp:lstrip(args["chars"]))
			elseif args["right"] then
				io.write(pp:rstrip(args["chars"]))
			end
		end
	end

end

if not pcall(debug.getlocal, 4, 1) then
	main()
end
