#!/bin/sh

# @(#) Reproduce random notes with urandom

parse() {
	setup REST help:usage -- 'Play random notes with your system'\''s /dev/urandom' ''

	msg -- 'Usage:' "  ${0##*/} [OPTIONS] [--] [aplay ARGUMENTS]" ''
	msg -- 'Options:'
	param Opt_n -n --notes -- \
		'List of notes to play as comma separated string of' \
		'numbers from 0 to 12. Default: 0,2,4,5,7,8,10,12'
	param Opt_d -d --nduration -- \
		'List of note durations as comma separated string of' \
		'numbers from 0 to 1. Default: 0.25,0.25,0.25,0.5,0.5,1'
	param Opt_v -v --volume -- 'Volume. Default: 100'
	param Opt_c -c --channels -- 'Number of channels. Default: 2'
	param Opt_r -r --rate -- 'Sampling rate in Hertz. Default: 16000'
	disp :usage -h --help -- 'Show this help'
}

eval "$(getoptions parse) exit 1"

: \
"${Opt_n:="0,2,4,5,7,8,10,12"}" \
"${Opt_d:="0.25,0.25,0.25,0.5,0.5,1"}" \
"${Opt_v:=100}" \
"${Opt_c:=2}" \
"${Opt_r:=16000}"

note_mangling() {
	# needs gawk
	awk -v volume="$Opt_v" -v notes="$Opt_n" \
		-v durations="$Opt_d" '{
split(notes,A,",")
split(durations,B,",")
for (i = 0; i < B[$2%7]; i += 0.0001)
	printf("%08X,", volume*sin(1382*2**(A[$1%9]/12)*i))
}'
}

hexdump -e '/1 "%u," /1 "%u "' /dev/urandom \
	| note_mangling \
	| xxd -r -p \
	| aplay -c "$Opt_c" -f S32_LE -r "$Opt_r" "$@"
