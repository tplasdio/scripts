<p align="center">
  <h1 align="center">Scripts</h1>
  <p align="center">
    My personal scripts
  </p>
</p>

- 🇪🇸 [Español](https://codeberg.org/tplasdio/scripts/src/branch/main/doc/README_es.md)

## Table of Contents

- [📰 Description](#description)
- [🚀 Installation](#installation)
- [📦 Dependencies](#dependencies)
- [⭐ Usage](#usage)
  - [🗄 Databases](#databases)
- [👀 See also](#see-also)
- [🤝 Contribute](#contribute)
- [🗣 Translation](#translation)
- [📝 Licence](#licence)

## 📰 Description

This is a collection of my personal scripts (mainly written in
POSIX shell) for Unix systems, and some specific for Linux distros
based on Arch.

## 🚀 Installation

It's usually enough to just copy the scripts, check and install their
dependencies, make them executables and run them, you can put them
somewhere in your `PATH`. Some scripts may require the libraries in
the `lib` directory.

What I recommend is set the `$XDG_DATA_HOME` environment varible to `~/.local/share`,
copy the files in the `lib` directory to `~/.local/lib`,
and the files in the `scripts` directory to `~/.local/bin` which you should put
in your `$PATH` environment variable. So something like:

```bash
git clone --recursive --filter=blob:none https://codeberg.org/tplasdio/scripts.git
mkdir -p ~/.local/{bin,lib,share}
cp scripts/scripts/* ~/.local/bin/
cp scripts/lib/* ~/.local/lib/
cp scripts/share/* ~/.local/share/

# ↓ Put these in your shell's configuration
export XDG_DATA_HOME=~/.local/share/
export PATH+=:~/.local/bin/
```

I'm still working on a Makefile and then I'll create a PKGBUILD to install them on Arch.
Some scripts are git submodules and you can install them independently,
see documentation in their respective pages (see the `submodules` directory).

## 📦 Dependencies

Since they are mainly shell scripts, they have dependencies from various places.
When packaging it I'll try to get the main ones included.
Many scripts have a comment at the beginning with the dependencies to check for.

The most important ones are [`fzf`](https://github.com/junegunn/fzf)
and [`getoptions.sh`](https://github.com/ko1nksm/getoptions).

The complete list will (*not yet*) be in the file 'deps.csv'

## ⭐ Usage

Most scripts show a help message with the `--help` option,
some will also have a manual `man <script>`, and some of
the smaller ones may have no documentation and you'll have to
read the source code.

Here is a short description of what most of them do:

Script | Short description
:---:|---
`abiertos` | Shows open files and X windows
`archivo` | Wrapper for archiving commands (e.g. tar, 7z, xz, gzip, zip)
`autocomp` | Automatic compilation
`bak` | Backup files: copies with correlative suffixes
`bateria` | Shows battery percentage
`bandaancha` | Record total uploaded and downloaded bytes
`bits` | Shows file as its bits (one and zero characters)
`bluefix` | Troubleshooting bluetooth commands
`caracruz` | Play heads or tails, many times with many players
`caracteres` | Special character selector
`change_date` | Change file's access, modification and changed time
`ch0` | Change ownership and permissions to only superuser
`cliptex` | Put a PNG image in the clipboard of current LaTeX text selection
`colores` | Show color palettes for your terminal (16,256,true,grayscale,etc.)
`compila` | Wrapper for compilers and interpreters (e.g. gcc, rustc, python, js78)
`crop.sh` | Wrapper for ffmpeg that cuts images
`dataframe` | Front-end of Python Pandas for conversion of table formats
`dem` | Command demonizer (leave commands running in the background)
`drag` | Drag (with the mouse) files to another directory
`dwapk` | Download APKs in Android phone to computer
`emoji` | Emoji selector
`enlace` | Open a URL link with a default app for the filetype, not with browser
`etiquetar` | Add metadata to audio files
`ffd` | Fuzzy file selector, with previews and ability to open them in editor
`find2var` | 'find' command, but prints quoted matches
`fman` | Manual and infodoc selector
`fondo-awesome` | Change my AwesomeWM wallpaper, with an optional logo
`frlwrap` | Wrapper for rlwrap that automatically adds autocompletions
`fuentes` | List installed fonts
`gpg-cs` | Safely symmetrically encrypt file with GPG
`gpg-cs-rm` | Safely symmetrically encrypt files with GPG with same password, shredding originals
`gpg-ds` | Safely symmetrically decrypt file with GPG
`gitd` | 'git' but specifying the date
`git-blame-someone-else` | Change author of a certain git commit. My improved fork
`grub-dark-matter_random.sh` | Change GRUB's backgroung picture to a random dark-matter theme one
`h` | Show function documentation of programming languages
`hex2rgb` | Convert hexadecimal colors to RGB
`infocmpp` | Show your terminal's capabilities, with description and colorful formatting
`iptab` | Show all your IPv4 and IPv6 iptables tables
`ixcpp` | Run a C++ REPL with 1 command. xcpp client and server at the same time
`jq-i` | Edit JSON's files with jq, but in place
`juliad` | Run a Julia server daemon and Julia scripts as clients to that server
`k` | Kill processes interactively
`lorem` | Filler text generator
`lrc-shift` | Change timestamps of .lrc lyrics files
`lsofp` | 'lsof' but passing only PIDs
`m4s` | Expand predefined m4 macros based on their extension
`macrandom.sh` | Randomize your MAC addresses
`magick-bg` | Generate random background images
`marcadores` | Select URLs from your browser's bookmarks
`marcagua` | Put a watermark to an image
`mata` | Quickly kill processes with most resource usage
`mv-swap` | Swap names of 2 files, with a native syscall
`nom.sh` | Show information of a file name, nicely formatted
`nspawn-arch.sh` | Create an Arch systemd-nspawn container from Arch
`num2ascii` | Convert numbers in different bases to ASCII characteres
`ocr` | Select screen region an OCR it, copying result to clipboard
`pacman-backup` | Generate backup files for pacman
`pacman-files` | List files provided by packages in pacman's database
`pacman-last` | List last files installed in pacman's database
`pacman-others` | List other commands from same packge owning certain command
`pacman-Qse` | List explicitly installed packages and short description
`pacman-Syu` | pacman -Syu but first updating Arch keyring
`pacman-Seu` | List explicitly installed upgradable packages
`pacman-which` | Show only which package owns a command
`pdfpass` | Password protect PDF
`pnglatex` | Generate a PNG image from a LaTeX formula
`poliglotar` | Automation of gettex commands to translate programs
`psel` | Select processes interactively
`pset` | Show related sets of processes
`punteros` | List cursor pointer configuration files for X (for Gtk, Qt)
`qr` | Select screen region to scan for QR or bar codes
`radios` | Select web radio to listen
`repl` | Select a REPL to open
`rgb2hex` | Convert RGB colors to hexadecimal
`rshred` | Use 'shred' recursively
`rutas` | List paths of different things/programs
`sed-rename` | Rename files using a `sed` expression
`sh0` | Invoke subshell that reads standard input but being able to pass it $0
`shlocker` | Auto screen locker + silencing + suspension + hibernation
`shtz` | Show datetimes for many timezones
`shvipe` | Edit text stream passed through a shell's pipe with your editor
`shellcode` | Shows file as shellcode (hexadecimal strings)
`subguionar` | Rename files changing spaces ' ' for underscores '\_'
`suspend-window` | Toggle suspension of a window
`teex`/`=` | Like `tee` but can write to the file being read like `sponge`
`terminal` | Show name of current terminal
`terminal-samewd` | Launch a new terminal in same directory
`tiempo` | Time operations, includes a clock, a stopwatch and a timer
`tonos` | Play random notes with your system's `/dev/urandom`
`ttyfont` | Change tty font interactively
`ttymap` | Change tty keymap interactively
`upapk` | Copy and install APKs from computer into Android phone
`urlkv` | List URL parameters as key-value pairs
`vcut` | Cut string characters like in bash
`wifis` | List available Wi-Fis connections
`wifimenu` | Wi-Fi menu selection. Connection and enabling/disabling
`winescript` | Run Windows batch scripts through Wine's cmd.exe
`xclipns` | Wrapper for clipboard commands, plus notifications
`xev-teclas` | Launch 'xev' and only look relevant key pressing information

*Note: Most selector/menu scripts can show options either through fzf, rofi or dmenu*

### 🗄 Databases

As a part for those scripts, there are some text databases in the
`share` directory that you may find useful:

Name | Short description
:---:|---
`icon_characters` | special characters like some Unicode groups and Nerd Fonts
`emojis` | emoji, short name and Unicode code
`currencies` | currency symbols, names, ISO 4217 acronym
`lorem` | latin words used in lorem generators
`radiosweb` | some web radios accesible through mpv
`terminfo_Caps` | ncurses terminal capabilities descriptions like in the terminfo(5) manpage

## 👀 See also
- [rofi-emoji](https://github.com/Mange/rofi-emoji): like the `emoji` script but a bit better for rofi
- [visidata](https://www.visidata.org): a table format converter like `dataframe` with more formats and a spreadsheet TUI
- [pacman utilities](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Utilities): other pacman scripts
- [moreutils](https://joeyh.name/code/moreutils/): includes `vipe` command as replacement for my `shvipe` and `vidir` to rename files in editor as alternative to `subguionar`

## 🤝 Contribute

You can open issues or send merge requests, optionally via `git bug`.
I do apologize for my inconsistency between writing code in Spanish and English.

## 🗣 Translation

I will rewrite the script messages to English and add translations
in Spanish with gettext so that others can contribute to translating
other languages in the PO files.

## 📝 Licence

AGPL-3.0-or-later

I prefer not to include a copyright notice in
most scripts since they are short.

